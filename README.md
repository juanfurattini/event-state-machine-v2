# Event State Machine #

Esta libreria sirve para controlar la secuencia de estados de un objeto.

### Componentes ###

* _**EventStateMachine:** Es la clase que representa la state machine. La misma provee los metodos necesarios para controlar el avance de estados_
* _**EventTransitionsTable:** Es la clase que guarda las transiciones de estados por evento. Las transiciones estan basadas en una relacion evento/estados_
* _**EventStateAssociationsTable:** Es la clase que guarda las asociaciones de los eventos de transicion y los estados a los cuales transiciona la maquina de estados_

### Componentes auxiliares ###

* _**TransitionsInterceptor:** Monitorea la actividad de las transiciones en la maquina de estados. Controla el avance y la adicion de transiciones_
* _**StateMachineStateInterceptor:** Monitorea el cambio de estado en la maquina de estados. Ejecuta los handlers de los estados al realizarse el cambio del mismo_
* _**StateableStateChangeInterceptor:** Monitorea el cambio de estado del objeto Stateable. Si el mismo cambia de estado por fuera de la maquina de estados es arrojada una excepcion_


### Como utilizarlo? ###

* Agregar la dependencia al proyecto
* Los objetos que contienen estado y que desean ser controlados por la state machine deben implementar la interface Stateable y agregar la state machine como atributo
* Configurar la maquina de estados asignandole transiciones
* Compilar la maquina de estados utilizando el metodo `compile()`
* Los estados a manejar por la aplicacion deben extender de la interface State
* Los eventos utilizados para el avance de estados deben extender de la interface Event
* Para avanzar de estado se debe llamar al metodo `transition(Event)` de la state machine


### Ejemplo ###

* Controlador:
```java
public class MyOwnStateableObjectController {

	public SomeResponse saveStateableObject(MyOwnStateableObject myObject) {
		myObject.changeState(MyEventClass.SAVE);
		// add your business logic here (i.e.: persist object in database)
	}

}
```

* Objeto concreto:
```java
public class MyOwnStateableObject implements Stateable {

	private State state;
	private EventStateMachine esm;

	public MyOwnStateableObject() {
		esm = new EventStateMachine(MyStateClass.DRAFT, this)
			.addTransition(MyStateClass.DRAFT, new EventStateAssociation(MyEventClass.SAVE, MyStateClass.STARTED))
			.addTransition(MyStateClass.STARTED, new EventStateAssociation(MyEventClass.FINALIZE, MyStateClass.FINALIZED))
			.compile();
	}

	@Override
	public State getState() {
		return state;
	}

	@Override
	public void setState(State state) {
		this.state = state;
	}

	public State changeState(Event event) {
		esm.transition(event);
	}

}
```

* Evento:
```java
public enum Events implements Event {

	SAVE,
	FINALIZE;

	@Override
	public String getName() {
		return this.name();
	}

}
```

* Estado:
```java
public enum MyStateClass implements State {

	DRAFT {
		@Override
		public void beforeStateChange(State to, Object... args) {
			// some clever code
		}

		@Override
		public void afterStateChange(Object... args) {
			// some clever code
		}
	},
	STARTED {
		@Override
		public void beforeStateChange(State to, Object... args) {
			// some clever code
		}

		@Override
		public void afterStateChange(Object... args) {
			// some clever code
		}
	},
	FINALIZED {
		@Override
		public void beforeStateChange(State to, Object... args) {
			// some clever code
		}

		@Override
		public void afterStateChange(Object... args) {
			// some clever code
		}
	};

	@Override
	public String getName() {
		return this.name();
	}
}
```


### Contacto ###

* [Mail](mailto:juan.furattini@gmail.com)
* [LinkedIn](https://www.linkedin.com/in/furattinijuan/)

-----------------------

*Juan Furattini*
package com.ellipticsoft.statemachine.eventsm.util;

import com.ellipticsoft.statemachine.eventsm.State;

public enum States implements State {

	DRAFT {
		@Override
		public void beforeStateChange(State to, Object... args) {
			// some clever code
		}

		@Override
		public void afterStateChange(Object... args) {
			// some clever code
		}
	},
	STARTED {
		@Override
		public void beforeStateChange(State to, Object... args) {
			// some clever code
		}

		@Override
		public void afterStateChange(Object... args) {
			// some clever code
		}
	},
	PAUSED {
		@Override
		public void beforeStateChange(State to, Object... args) {
			// some clever code
		}

		@Override
		public void afterStateChange(Object... args) {
			// some clever code
		}
	},
	CANCELED {
		@Override
		public void beforeStateChange(State to, Object... args) {
			// some clever code
		}

		@Override
		public void afterStateChange(Object... args) {
			// some clever code
		}
	},
	FINALIZED {
		@Override
		public void beforeStateChange(State to, Object... args) {
			// some clever code
		}

		@Override
		public void afterStateChange(Object... args) {
			// some clever code
		}
	};

	@Override
	public String getName() {
		return this.name();
	}
}

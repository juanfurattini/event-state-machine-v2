package com.ellipticsoft.statemachine.eventsm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import com.ellipticsoft.statemachine.eventsm.exceptions.ClosedStateMachineException;
import com.ellipticsoft.statemachine.eventsm.exceptions.IllegalStateModificationException;
import com.ellipticsoft.statemachine.eventsm.exceptions.StateCorruptedException;
import com.ellipticsoft.statemachine.eventsm.exceptions.StateMachineNotCompiledException;
import com.ellipticsoft.statemachine.eventsm.util.Events;
import com.ellipticsoft.statemachine.eventsm.util.Request;
import com.ellipticsoft.statemachine.eventsm.util.States;

public class EventStateMachineTest {

	private Request request;
	private EventStateMachine esm;

	@Before
	public void setUp() throws Exception {
		request = new Request();
	}

	@Test
	public void whenMachineIsCreatedTheInitialStateWillBeSettedByTheConstructor() {
		esm = new EventStateMachine(States.DRAFT, request)
				.addTransition(States.DRAFT, new EventStateAssociation(Events.GENERATE, States.STARTED)).compile();
		assertEquals(States.DRAFT, esm.getState());
	}

	@Test
	public void whenMachineIsCreatedTheStateableObjectAndTheMachineWillHaveTheSameInitialState() {
		esm = new EventStateMachine(States.DRAFT, request)
				.addTransition(States.DRAFT, new EventStateAssociation(Events.GENERATE, States.STARTED)).compile();
		assertEquals(esm.getState(), request.getState());
	}

	@Test
	public void whenTransitionIsInvokedWithAnExistentTransitionEventWithAssociatedStateItMustTransit() {
		esm = new EventStateMachine(States.DRAFT, request)
				.addTransition(States.DRAFT, new EventStateAssociation(Events.GENERATE, States.STARTED)).compile();
		esm.transition(Events.GENERATE);
		assertEquals(States.STARTED, esm.getState());
	}

	@Test
	public void whenTransitionIsInvokedTheStateableObjectAndTheMachineWillHaveTheSameState() {
		esm = new EventStateMachine(States.DRAFT, request)
				.addTransition(States.DRAFT, new EventStateAssociation(Events.GENERATE, States.STARTED)).compile();
		esm.transition(Events.GENERATE);
		assertEquals(esm.getState(), request.getState());
	}

	@Test
	public void whenAddTransitionToACompiledMachineAnClosedStateMachineExceptionMustBeThrown() {
		esm = new EventStateMachine(States.DRAFT, request).compile();
		try {
			esm.addTransition(States.DRAFT, new EventStateAssociation(Events.GENERATE, States.STARTED));
			fail("An ClosedStateMachineException must be thrown");
		} catch (ClosedStateMachineException e) {
			assertTrue(true);
		}
	}

	@Test
	public void whenTransitionIsInvokedWithANonCompiledMachineAnStateMachineNotCompiledExceptionMustBeThrown() {
		esm = new EventStateMachine(States.DRAFT, request).addTransition(States.DRAFT,
				new EventStateAssociation(Events.GENERATE, States.STARTED));
		try {
			esm.transition(Events.GENERATE);
			fail("An StateMachineNotCompiledException must be thrown");
		} catch (StateMachineNotCompiledException e) {
			assertTrue(true);
		}
	}

	@Test
	public void whenStateableStateIsChangedFromOutsideTheMachineIsInvokedAnIllegalStateModificationExceptionMustBeThrown() {
		esm = new EventStateMachine(States.DRAFT, request)
				.addTransition(States.DRAFT, new EventStateAssociation(Events.GENERATE, States.STARTED))
				.addTransition(States.STARTED, new EventStateAssociation(Events.FINALIZE, States.FINALIZED)).compile();
		esm.transition(Events.GENERATE);
		try {
			request.setState(States.DRAFT);
			fail("An IllegalStateModificationException must be thrown");
		} catch (IllegalStateModificationException e) {
			assertTrue(true);
		}
	}

	@Test
	public void whenStateableStateIsChangedFromOutsideTheMachineWithAHookAndTransitionIsInvokedAnStateCorruptedExceptionMustBeThrown() {
		esm = new EventStateMachine(States.DRAFT, request)
				.addTransition(States.DRAFT, new EventStateAssociation(Events.GENERATE, States.STARTED))
				.addTransition(States.STARTED, new EventStateAssociation(Events.FINALIZE, States.FINALIZED)).compile();
		esm.transition(Events.GENERATE);
		request.hookState(States.DRAFT);
		try {
			esm.transition(Events.FINALIZE);
			fail("An StateCorruptedException must be thrown");
		} catch (StateCorruptedException e) {
			assertTrue(true);
		}
	}

}

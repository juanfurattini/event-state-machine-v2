package com.ellipticsoft.statemachine.eventsm;

import java.util.HashMap;
import java.util.Map;

import com.ellipticsoft.statemachine.eventsm.exceptions.NoStateTransitionsException;

/**
 * 
 * @author juan.m.furattini
 *
 */
public final class EventTransitionsTable {

	private Map<State, EventStateAssociationsTable> transitions = new HashMap<State, EventStateAssociationsTable>();

	/**
	 * 
	 * @param from
	 * @param to
	 */
	@SafeVarargs
	public final void addTransition(State from, EventStateAssociation... to) {
		transitions.put(from, new EventStateAssociationsTable(to, from));
	}

	/**
	 * 
	 * @param state
	 * @return
	 * @throws NoStateTransitionsException
	 */
	public final EventStateAssociationsTable getTransitionsOf(State state) throws NoStateTransitionsException {
		if (!transitions.containsKey(state)) {
			throw new NoStateTransitionsException("There is no transitions for state " + state.getName());
		}
		return transitions.get(state);

	}
}

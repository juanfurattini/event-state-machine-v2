package com.ellipticsoft.statemachine.eventsm;

/**
 * 
 * @author juan.m.furattini
 *
 */
public interface Stateable {

	State getState();

	void setState(State state);

}

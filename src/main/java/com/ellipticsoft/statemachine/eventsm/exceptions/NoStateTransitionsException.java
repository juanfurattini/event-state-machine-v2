package com.ellipticsoft.statemachine.eventsm.exceptions;

public class NoStateTransitionsException extends RuntimeException {

	private static final long serialVersionUID = 5167331226744555336L;

	public NoStateTransitionsException(String message) {
		super(message);
	}

}

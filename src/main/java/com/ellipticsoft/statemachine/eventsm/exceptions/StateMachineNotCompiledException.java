package com.ellipticsoft.statemachine.eventsm.exceptions;

public class StateMachineNotCompiledException extends RuntimeException {

	private static final long serialVersionUID = 8610268059543148154L;

	public StateMachineNotCompiledException(String message) {
		super(message);
	}

}

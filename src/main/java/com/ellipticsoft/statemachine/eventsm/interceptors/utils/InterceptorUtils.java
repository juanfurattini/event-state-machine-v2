package com.ellipticsoft.statemachine.eventsm.interceptors.utils;

import org.aspectj.lang.JoinPoint;

import com.ellipticsoft.statemachine.eventsm.EventStateMachine;

/**
 * 
 * @author juan.m.furattini
 *
 */
public class InterceptorUtils {

	/**
	 * 
	 * @param point
	 * @return
	 */
	public static EventStateMachine getStateMachine(JoinPoint point) {
		return EventStateMachine.class.cast(point.getThis());
	}

}

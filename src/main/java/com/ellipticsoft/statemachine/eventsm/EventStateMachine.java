package com.ellipticsoft.statemachine.eventsm;

/**
 * 
 * @author juan.m.furattini
 *
 */
public final class EventStateMachine {

	private State state;
	private Stateable stateable;

	private boolean compiled = false;

	private EventTransitionsTable table = new EventTransitionsTable();

	public EventStateMachine(final State initialState, final Stateable stateable) {
		this.state = stateable.getState();
		this.stateable = stateable;
		if (stateable.getState() == null) {
			this.state = initialState;
			this.stateable.setState(initialState);
		}
	}

	public State getState() {
		return state;
	}

	public Stateable getStateable() {
		return stateable;
	}

	public boolean isCompiled() {
		return compiled;
	}

	/**
	 * 
	 * @param from
	 * @param to
	 * @return
	 */
	@SafeVarargs
	public final EventStateMachine addTransition(State from, EventStateAssociation... to) {
		table.addTransition(from, to);
		return this;
	}

	/**
	 * 
	 * @param event
	 * @return
	 */
	public State transition(Event event) {
		State nextState = table.getTransitionsOf(stateable.getState()).transitionFor(event);
		updateState(nextState);
		return state;
	}

	/**
	 * 
	 * @return
	 */
	public EventStateMachine compile() {
		compiled = true;
		return this;
	}

	/**
	 * 
	 * @param nextState
	 * @return
	 */
	private State updateState(State nextState) {
		state = nextState;
		stateable.setState(state);
		return state;
	}

}

package com.ellipticsoft.statemachine.eventsm;

import java.util.HashMap;
import java.util.Map;

import com.ellipticsoft.statemachine.eventsm.exceptions.NoStateTransitionsException;

/**
 * 
 * @author juan.m.furattini
 *
 */
public final class EventStateAssociationsTable {

	private final State state;
	private Map<Event, State> associations = new HashMap<Event, State>();

	public EventStateAssociationsTable(EventStateAssociation[] to, State from) {
		state = from;
		for (EventStateAssociation association : to) {
			associations.put(association.getEvent(), association.getState());
		}
	}

	/**
	 * 
	 * @param event
	 * @return
	 * @throws NoStateTransitionsException
	 */
	public final State transitionFor(Event event) throws NoStateTransitionsException {
		if (!associations.containsKey(event)) {
			throw new NoStateTransitionsException(
					"There is no transitions for state " + state.getName() + " with event " + event.getName());
		}
		return associations.get(event);
	}

}
